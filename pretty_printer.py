import re


class PrettyPrinter:
    # to make them bold, add `1;` before the colour ID
    GREY = '\033[90m'
    WHITEBOLD = "\033[1;37m"
    PURPLE = '\033[95m'
    BLUE = '\033[1;94m'
    GREEN = '\033[92m'
    YELLOW = '\033[93m'
    RED = '\033[91m'

    NO_FORMAT = '\033[0m'

    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

    __debug_on = False
    __trace_on = False
    __wrap_length = 0

    _icons = {
        "ok":     ["+", "\U0000263A"],
        "info":   ["*", "\U0001F914"],
        "warn":   ["~", "\U0001F644"],
        "error":  ["!", "\N{white frowning face}"],
        "debug":  ["-", "\U0001F914"],
        "trace":  [" ", "\U0001F910"],
        "report": [">", "\U0001F610"]
    }

    def __init__(self, debug=True, trace=False, wrap=0, emoji=False):
        self.__debug_on = debug or trace
        self.__trace_on = trace
        self.__wrap_length = wrap
        self.__emoji = emoji

    def ok(self, msg):
        self.print_col("ok", self.__wrap(msg), self.GREEN)

    def info(self, msg):
        self.print_col("info", self.__wrap(msg), self.BLUE)

    def warn(self, msg):
        self.print_col("warn", self.__wrap(msg), self.YELLOW)

    def error(self, msg, exception: Exception = None):
        if exception:
            self.print_col("error", self.__wrap(f"{msg}\n{getattr(exception, 'message', str(exception))}"), self.RED)
        else:
            self.print_col("error", self.__wrap(msg), self.RED)

    def output(self, msg):
        self.print_col("report", self.__wrap(msg), self.PURPLE)

    def debug(self, msg):
        if self.__debug_on:
            self.print_col("debug", self.__wrap(msg), self.WHITEBOLD)

    @property
    def is_debug(self):
        return self.__debug_on

    def trace(self, msg):
        if self.__trace_on:
            self.__default("[ ] {}".format(self.__wrap(msg)))

    @property
    def is_trace(self):
        return self.__trace_on

    def print_col(self, str1, str2, col):
        print("{}[{}]{} {}".format(col, self._icons[str1][1 if self.__emoji else 0], self.NO_FORMAT, str2))

    def __default(self, msg):
        print(msg)

    def __wrap(self, msg):
        if self.__wrap_length == 0:
            return msg
        out = []
        wl = self.__wrap_length - 4  # "4" because of the "[x] "
        for line in msg.split('\n'):
            tmp = line.strip()
            while len(tmp) > wl:
                # Try for a space to split on first so we don't mess up IP addresses and domains
                r = re.search(r"[\s]", tmp[wl - 1:])
                if r is not None:
                    i = r.start() + wl
                    out.append(tmp[:i].strip())
                    tmp = tmp[i:].strip()
                else:
                    if len(tmp) > wl + 15:  # bit of a buffer to try and stop splitting the last word
                        # just hard split :(
                        out.append(tmp[:wl].strip())
                        tmp = tmp[wl:].strip()
                    else:
                        break
            out.append(tmp)
        # 4 spaces to accommodate "[x] "
        return "\n    ".join(out)
