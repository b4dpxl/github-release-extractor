import argparse
import json
import requests

from bs4 import BeautifulSoup
from pretty_printer import PrettyPrinter

parser = argparse.ArgumentParser("GitHub Release Grabber", description="Grab release history for a GitHub project")
parser.add_argument("-v", "--verbose", dest="verbose", help="Verbose output", required=False, action="store_true")
parser.add_argument(dest='project', help="Project name. E.g. 'jquery/jquery'")
args = parser.parse_args()

printer = PrettyPrinter(debug=args.verbose)

results = {}
url = f"https://github.com/{args.project}/releases"
while url:
    printer.debug(f"Fetching {url}")
    res = requests.get(url)
    soup = BeautifulSoup(res.content, 'html.parser')
    for div in soup.select("div.release-entry"): # could also use soup.find_all("div", class_="release-entry")
        timestamp = div.select("relative-time")[0]["datetime"]
        if len(div.select("h4.commit-title")):
            version = div.select("h4.commit-title a")[0].get_text().strip()
        elif len(div.select("div.release-header a")):
            version = div.select("div.release-header a")[0].get_text().strip()
        else:
            printer.warn("Unable to extract version")
            continue
        results[version] = timestamp

    next_page = soup.find_all("a", string="Next")
    if next_page:
        url = next_page[0]["href"]
    else:
        break

printer.output("Results:\n" + json.dumps(results, indent=4))
